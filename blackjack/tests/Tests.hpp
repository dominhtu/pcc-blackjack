
#ifndef BLACKJACK_TESTS_HPP
#define BLACKJACK_TESTS_HPP


#include "../Headers/Deck.hpp"
#include "../Headers/Player.hpp"
#include "../Headers/Dealer.hpp"
#include <cassert>
#include <iostream>
#define ASSERT(condition) \
    do { \
        if (!(condition)) { \
            std::cerr << "Test " << testCounter << ": failed" << std::endl; \
        } else { \
            std::cout << "Test " << testCounter << ": OK" << std::endl; \
        } \
        testCounter++; \
    } while (false)

class Tests {
private:
    int testCounter = 1;
public:
    void playerHitCardTest();
    void aceTurnsIntoOneAfterDrawingBustingHandTest();
    void refillDeckTest();
};


#endif //BLACKJACK_TESTS_HPP
