#include <iostream>
#include "Headers/Game.hpp"

int main( int argc, char* argv[] )
{
    if( argc == 1 )
    {
        Game game;
        game.start();

    }
    else if( argc == 2 )
    {
        for( int i = 1; i < argc; ++i )
        {
            std::string arg = argv[i];
            if( arg == "--help" )
            {
                std::cout << "\x1B[2J\x1B[H" << std::endl;
                std::cout << "-------------------------------------- BLACKJACK --------------------------------------" << std::endl;
                std::cout << "|  Equally well known as Twenty-One, the object of the game is to beat the dealer by  |" << std::endl;
                std::cout << "|  getting a count as close to 21 as possible without going over 21.                  |" << std::endl;
                std::cout << "|  All face cards are 10, ace is worth 1 or 11 and any other card is its pip value.   |" << std::endl;
                std::cout << "|                                                                                     |" << std::endl;
                std::cout << "|  BETTING: Before the deal begins player places a bet in chips (starts with 1000)    |" << std::endl;
                std::cout << "|                                                                                     |" << std::endl;
                std::cout << "|  THE PLAY: Player will be dealt two cards, and decide whether to stand or hit.      |" << std::endl;
                std::cout << "|  Standing is not asking for another card, while hitting means asking for another    |" << std::endl;
                std::cout << "|  card, one at a time, until deciding to stand on the total (if it is under 21)      |" << std::endl;
                std::cout << "|  or goes \"bust\" (if it is over 21). In the latter case the player loses.          |" << std::endl;
                std::cout << "|  Player can also double down on their bet. Player will place a bet equal to the     |" << std::endl;
                std::cout << "|  original bet, and the dealer gives the player just one card.                       |" << std::endl;
                std::cout << "|  The combination of an ace with a card other than a ten-card is known as a          |" << std::endl;
                std::cout << "|  \"soft hand\", because the player can count hte ace as a 1 or 11, and either draw  |" << std::endl;
                std::cout << "|  cards or not. For example with a \"soft 17\" (an ace and a 6), the total is 7 or   |" << std::endl;
                std::cout << "|  17. If the draw creates a bust hand by counting the ace as an 11, the player       |" << std::endl;
                std::cout << "|  simply counts the ace as a 1 and continues playing by standing or hitting.         |" << std::endl;
                std::cout << "|                                                                                     |" << std::endl;
                std::cout << "|  DEALER PLAY: The dealer turns second card face up. If the total is 17 or more it   |" << std::endl;
                std::cout << "|  must stand. if the total is 16 or under, they must take a card. The dealer must    |" << std::endl;
                std::cout << "|  continue to take cards until the total is 17 or more, at which point the dealer    |" << std::endl;
                std::cout << "|  must stand. If the dealer has an ace and counting it as 11 would bring the total   |" << std::endl;
                std::cout << "|  to 17 or more (but not over 21) the dealer must count the ace as 11 and stand.     |" << std::endl;
                std::cout << "---------------------------------------------------------------------------------------" << std::endl;
            }
        }

    }

    return 0;
}
