#include "../Headers/Game.hpp"
#include <iostream>

#include <climits>

#include <thread>

void Game::start()
{
    do
    {
        // ------------------------------- STAGE 1 -------------------------------
        resetRound();
        char userInput = 0;
        int betAmount = 0;

        if( player.getChips() <= 0 )
        {
            std::cout << "You are out of chips! STOP GAMBLING!" << std::endl;
            return;
        }

        // read input for bet amount
        std::cout << "How much do you want to bet? (currently have: " + std::to_string(player.getChips()) + ")" << std::endl;
        while( !(std::cin >> betAmount) || betAmount <= 0 || betAmount > player.getChips() )
        {
            std::cin.clear();
            std::cin.ignore( INT_MAX, '\n' );
            std::cout << "Invalid input." << std::endl;
        }
        player.hit( deck.dealCard() );
        dealer.hit( deck.dealCard() );
        player.hit( deck.dealCard() );

        dealer.getHandAndValue();
        std::cout << std::endl;
        player.getHandAndValue();

        dealer.hit( deck.dealCard() );



        // ------------------------------- STAGE 2 -------------------------------
        while( userInput != 's' && player.getHandVal() < 21 )
        {
            readInput( userInput );

            if( userInput == 'd' && player.getAlreadyHit() )
            {
                std::cout << "You can't double down after you already hit." << std::endl;
                continue;
            }

            if( userInput == 'd' && !player.getAlreadyHit() )
            {
                if( betAmount * 2 > player.getChips() )
                {
                    std::cout << "You can't double down with current amount of chips (" + std::to_string(player.getChips()) + ")" << std::endl;
                    continue;
                }
                betAmount *= 2;
                player.hit( deck.dealCard() );
                player.getHandAndValue();
                if( player.getHandVal() > 21 )
                    std::cout << "YOU BUSTED!!" << std::endl;

                break;
            }

            if( userInput == 'h' )
            {
                player.setAlreadyHit();
                player.hit( deck.dealCard() );
                player.getHandAndValue();

                if( player.getHandVal() > 21 )
                {
                    std::cout << "YOU BUSTED!!" << std::endl;
                    break;
                }
            }
        }
        // -----------------------------------------------------------------------

        // ------------------------------- STAGE 3 -------------------------------
        // dealer hitting stage
        std::cout << std::endl;
        dealer.getHandAndValue();
        while( dealer.getHandVal() < 17 && player.getHandVal() <= 21 )
        {
            dealer.hit( deck.dealCard() );
            std::this_thread::sleep_for(std::chrono::seconds(2));
            std::cout << std::endl;
            dealer.getHandAndValue();
        }
        // -----------------------------------------------------------------------


        // ------------------------------- STAGE 4 -------------------------------
        switch( checkWin() )
        {
            case 1:
                std::cout << "You won!" << std::endl;
                player.winBet( betAmount );
                break;
            case -1:
                std::cout << "You lost!" << std::endl;
                player.loseBet( betAmount );
                break;
            case 0:
                std::cout << "You tied!" << std::endl;
                break;
        }
        // -----------------------------------------------------------------------


        // ------------------------------- STAGE 5 -------------------------------
        // prompt for another round
        std::cout << "Want to play another round? (Y/N)" << std::endl;
        while( !(std::cin >> userInput) | (tolower(userInput) != 'y' && tolower(userInput) != 'n') )
        {
            std::cin.clear();
            std::cin.ignore( CHAR_MAX, '\n' );
            std::cout << "Invalid input." << std::endl;
        }
        if( userInput == 'n' )
            quit = true;

    } while( !quit );

}

int Game::checkWin()
{
    int dealerVal = dealer.getHandVal();
    int playerVal = player.getHandVal();

    if( playerVal > 21 || (dealerVal <= 21 && dealerVal > playerVal ) )
        return -1;
    if( dealerVal > 21 || playerVal > dealerVal )
        return 1;
    return 0;

}

void Game::readInput( char& userInput )
{

    std::cout << "Do you want to (h)it, (s)tand or (d)ouble down?" << std::endl;
    while( !(std::cin >> userInput) || (tolower(userInput) != 'h' && tolower(userInput) != 's' && tolower(userInput) != 'd') )
    {
        std::cin.clear();
        std::cin.ignore( CHAR_MAX, '\n' );
        std::cout << R"(Invalid input. (Put 'h', 's' or 'd'))" << std::endl;
    }
}

void Game::resetRound()
{
    dealer.resetParticipant();
    player.resetParticipant();
    player.resetPlayer();
}


