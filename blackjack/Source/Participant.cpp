#include "../Headers/Participant.hpp"

#include <iostream>

void Participant::hit( const Card& card )
{
    handVal += card.getValue();
    if( card.getRank() == "ACE" )
    {
        hasAce = true;
    }
    if( hasAce && handVal > 21 && !changedAce )
    {
        changedAce = true;
        handVal -= 10;
    }
    hand.emplace_back( card );
}


int Participant::getHandVal() const
{
    return handVal;
}

bool Participant::getChangedAce() const
{
    return changedAce;
}

std::vector<Card> Participant::getHand() const
{
    return hand;
}

void Participant::clearHand()
{
    hand.clear();
}

void Participant::resetHandVal()
{
    handVal = 0;
}

void Participant::resetChangedAce()
{
    changedAce = false;
}

void Participant::resetHasAce()
{
    hasAce = false;
}

void Participant::resetParticipant()
{
    clearHand();
    resetHandVal();
    resetChangedAce();
    resetHasAce();
}


void Participant::printHand() const
{
    for( int i = 0; i < hand.size(); ++i )
    {
        if( i ) // after 0th element print commas BEFORE printing out the card
            std::cout << ", ";
        std::cout << hand[i];
    }
    std::cout << std::endl;
}

void Participant::printHandValue() const
{
    // has ace and still CAN be changed
    if( hasAce && !changedAce )
        std::cout << "(" << handVal - 10 << "/" << handVal << ")" << std::endl;
    // ace ALREADY CHANGED or does NOT HAVE ace
    else
        std::cout << handVal << std::endl;
}


