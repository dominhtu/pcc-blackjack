#include "../Headers/Player.hpp"
#include <iostream>
void Player::getHandAndValue() const
{
    std::cout << "Your hand:" << std::endl;
    printHand();
    std::cout << "Your value: ";
    printHandValue();
}


int Player::getChips() const
{
    return chips;
}

void Player::winBet(int betAmount)
{
    chips += betAmount;
}

void Player::loseBet(int betAmount)
{
    chips -= betAmount;
}

void Player::resetPlayer()
{
    alreadyHit = false;
}

bool Player::getAlreadyHit() const
{
    return alreadyHit;
}

void Player::setAlreadyHit()
{
    alreadyHit = true;
}
