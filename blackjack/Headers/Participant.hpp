#pragma once
#include "Card.hpp"

#include <vector>

class Participant
{
protected:
    std::vector<Card> hand;
    int handVal = 0;
    bool changedAce = false;
    bool hasAce = false;
public:
    void hit( const Card& card );
    int getHandVal() const;
    bool getChangedAce() const; // might not need this lol mb in game window later
    void clearHand();
    void resetHandVal();
    void resetChangedAce();
    void resetHasAce();
    void resetParticipant();
    void printHand() const;
    void printHandValue() const;
    virtual void getHandAndValue() const = 0;
    std::vector<Card> getHand() const; // debug purpose
};