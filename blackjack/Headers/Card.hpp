#pragma once
#include <string>


class Card
{
private:
    std::string m_Rank;
    int m_Value;
    std::string m_Suit;
public:
    Card() = default;
    Card( const std::pair<std::string, int>& rank, const std::string& suit );
    std::string getRank() const;
    std::string getSuit() const;
    int getValue() const;


    friend std::ostream & operator<<( std::ostream& os, const Card& card );
};
