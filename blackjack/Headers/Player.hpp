#include "Participant.hpp"

class Player : public Participant
{
private:
    int chips = 1000;
    bool alreadyHit = false;
public:
    int getChips() const;
    void winBet( int betAmount );
    void loseBet( int betAmount );
    void resetPlayer();
    bool getAlreadyHit() const;
    void setAlreadyHit();
    void getHandAndValue() const override;
};
