#include "../Headers/Deck.hpp"

#include <algorithm>
#include <random>
#include <chrono>
#include <iostream>


Deck::Deck()
{
    refillDeck();
    shuffleCards();
}

void Deck::refillDeck()
{
    std::cout << " ------------------ REFILLING DECK ------------------" << std::endl;
    for( const auto& suit : en_Suit )
    {
        for( const auto& rank : en_Rank )
            cards.emplace_back( rank, suit );
    }
}

void Deck::shuffleCards()
{
    auto seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::shuffle( cards.begin(), cards.end(), std::default_random_engine( seed ) );
}

Card Deck::dealCard()
{
    Card card;
    if( cards.empty() )
    {
        refillDeck();
        shuffleCards();
    }
    card = cards.back();
    cards.pop_back();

    return card;
}

std::vector<Card> Deck::getCards() const
{
    return cards;
}

