#include "Deck.hpp"
#include "Player.hpp"
#include "Dealer.hpp"

class Game
{
private:
    bool quit = false;
    Deck deck;
    Player player;
    Dealer dealer;
public:
    Game() = default;
    void start();
    int checkWin();
    static void readInput( char& userInput );
    void resetRound();
};