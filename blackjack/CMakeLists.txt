cmake_minimum_required(VERSION 3.26)
project(blackjack)

set(CMAKE_CXX_STANDARD 17)

add_executable(blackjack main.cpp
        Headers/Participant.hpp
        Headers/Card.hpp
        Headers/Deck.hpp
        Headers/Dealer.hpp
        Headers/Player.hpp
        Headers/Game.hpp
        Source/Card.cpp
        Source/Game.cpp
        Source/Deck.cpp
        Source/Participant.cpp
        Source/Player.cpp
        Source/Dealer.cpp
)

add_executable(tests tests/Tests.cpp
        Headers/Participant.hpp
        Headers/Card.hpp
        Headers/Deck.hpp
        Headers/Dealer.hpp
        Headers/Player.hpp
        Headers/Game.hpp
        Source/Card.cpp
        Source/Game.cpp
        Source/Deck.cpp
        Source/Participant.cpp
        Source/Player.cpp
        Source/Dealer.cpp
        tests/Tests.hpp
)
