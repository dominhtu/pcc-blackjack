#include "../Headers/Dealer.hpp"
#include <iostream>

void Dealer::getHandAndValue() const
{
    std::cout << "Dealer's hand:" << std::endl;
    printHand();
    std::cout << "Dealer's value: ";
    printHandValue();
}
