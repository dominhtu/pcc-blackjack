#include "../Headers/Card.hpp"

Card::Card(const std::pair<std::string, int>& rank, const std::string &suit)
{
    m_Rank = rank.first;
    m_Value = rank.second;
    m_Suit = suit;
}

int Card::getValue() const
{
    return m_Value;
}

std::string Card::getRank() const
{
    return m_Rank;
}

std::string Card::getSuit() const
{
    return m_Suit;
}

std::ostream &operator<<( std::ostream &os, const Card &card )
{
    os << card.getRank() + " of " + card.getSuit();
    return os;
}