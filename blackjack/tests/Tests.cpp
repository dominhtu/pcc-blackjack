
#include "Tests.hpp"


void Tests::playerHitCardTest()
{
    Player player;
    Card card1( {"ACE", 11}, "HEARTS" );
    Card card2( {"KING", 10}, "SPADES" );
    Card card3( {"THREE", 3}, "DIAMONDS" );
    std::vector<Card> deckSimulation = { card1, card2, card3 };


    player.hit( deckSimulation[0] );
    player.hit( deckSimulation[1] );
    player.hit( deckSimulation[2] );
    ASSERT( player.getHand().size() == 3 );
    ASSERT( player.getHand().at(0).getSuit() == "HEARTS" );
    ASSERT( player.getHand().at(1).getRank() == "KING" );
    ASSERT( player.getHand().at(2).getValue() == 3 );
}

void Tests::aceTurnsIntoOneAfterDrawingBustingHandTest()
{
    Player player;
    Card card1( {"ACE", 11}, "HEARTS" );
    Card card2( {"KING", 10}, "SPADES" );
    Card card3( {"THREE", 3}, "DIAMONDS" );
    std::vector<Card> deckSimulation = { card1, card2, card3 };

    player.hit( deckSimulation[0] );
    player.hit( deckSimulation[2] );
    ASSERT( player.getHandVal() == 14 );
    player.hit( deckSimulation[1] );
    ASSERT( player.getHandVal() == 14 );
}

void Tests::refillDeckTest()
{
    Deck deck;
    Player player;
    ASSERT( deck.getCards().size() == 52 );
    player.hit( deck.dealCard() );
    player.hit( deck.dealCard() );
    player.hit( deck.dealCard() );
    ASSERT( deck.getCards().size() == 49 );
    while( deck.getCards().size() != 0 )
        deck.dealCard();
    deck.refillDeck();
    ASSERT( deck.getCards().size() == 52 );
}

int main()
{
    Tests tests;
    std::cout << "Player hit card test:" << std::endl;
    tests.playerHitCardTest();
    std::cout << "Ace turns into ONE after drawing a busting hand test:" << std::endl;
    tests.aceTurnsIntoOneAfterDrawingBustingHandTest();
    std::cout << "Refill deck test:" << std::endl;
    tests.refillDeckTest();
}